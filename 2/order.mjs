export default class Order {
    constructor(customerId, ...productIds) {
        this.customerId = customerId;
        this.productIds = productIds
    }
}