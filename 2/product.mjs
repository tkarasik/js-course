export default class Product {
    constructor(id, name, itemsInStock = 0) {
        this.id = id;
        this.name = name;
        this.itemsInStock = itemsInStock;
    }

    decrementQuantity() {
        if (this.itemsInStock === 0) {
            throw `Product ${name} (id ${id}) is out of stock`;
        }
        this.itemsInStock--;
    }
}