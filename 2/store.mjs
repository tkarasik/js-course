import Product from "./product.mjs";
import Customer from "./customer.mjs";
import Order from "./order.mjs";

export default class Store {

    constructor() {
        this.customers = new Map();
        this.products = new Map();
        this.orders = new Array();
    }

    addCustomer(id, name, address) {
        if (this.customers.has(id)) {
            throw `Customer id ${id} already exists`;
        }
        this.customers.set(id, new Customer(id, name, address));
    }

    addProduct(id, name, itemsInStock) { 
        if (this.products.has(id)){
            throw `Product id ${id} already exists`;
        }
        this.products.set(id, new Product(id, name, itemsInStock));
    }

    #validateOrder = (customerId, ...productIds) => {
        if (!this.customers.has(customerId)){
            throw `Customer id ${customerId} doesn't exist - cannot complete order`;
        }
        for (let productId of productIds) {
            let product = this.products.get(productId);
            if (!product) {
                throw `Product id ${productId} doesn't exist - cannot complete order`;
            }
            if (product.itemsInStock === 0) {
                throw `Product id ${productId} is out of stock - cannot complete order`;
            }
        }
    }

    #addOrderInner = (customerId, ...productIds) => {
        let order = new Order(customerId, ...productIds);
        this.orders.push(order);
    }

    addOrder(customerId, ...productIds) {
        this.#validateOrder(customerId, ...productIds);

        for (let productId of productIds) {
            this.products.get(productId).decrementQuantity();
        }   
        
        this.#addOrderInner(customerId, ...productIds);
    }

    printOrders() {
        this.orders.forEach((order) => {
            let customer = this.customers.get(order.customerId);
            let productNames = [...order.productIds].map(productId => this.products.get(productId)).map(product => `${product.name} (${product.id})`);
            console.log(`Customer ${customer.name} (${customer.id}) ordered products: ${productNames.join(", ")}`);
        })
    }

    save() {
        console.log(JSON.stringify({
            customers: [...this.customers.values()],
            products: [...this.products.values()],
            orders: this.orders
        }));
    }

    load(state) {
        let storeData, error;

        try {
            storeData = JSON.parse(state);
        } catch (ex) {
            error = ex;
        }
        
        if (!storeData || !storeData.customers || !storeData.products || !storeData.orders) {
            throw "Invalid store data" + (error ? `: ${error}` : "");
        }

        for (let customerId in storeData.customers) {
            let customer = storeData.customers[customerId];
            this.addCustomer(customer.id, customer.name, customer.address);
        }
        for (let productId in storeData.products) {
            let product = storeData.products[productId];
            this.addProduct(product.id, product.name, product.itemsInStock);
        }
        storeData.orders.forEach((order) => {
            this.#addOrderInner(order.customerId, ...order.productIds);
        });
    }

    notify() {
        this.stockInterval = setInterval(() => {
            let productsOutOfStock = [...this.products.values()].filter(product => product.itemsInStock === 0).map(product => `${product.name} (${product.id})`) ;
            console.log(`Products out of stock: ${productsOutOfStock.join(", ")}`)
        }, 10000);
    }
}