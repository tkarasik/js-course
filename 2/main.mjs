import Store from "./store.mjs";

let store = new Store();

//#region Option 1
// store.addCustomer(123, "Liat", "Jerusalem");
// store.addCustomer(234, "Tomer", "Giv'at Shmuel");
// store.addCustomer(345, "Ma'ayan", "Tel Aviv");
// store.addCustomer(456, "Inbar", "Ra'anana");

// store.addProduct(1, "Milk", 2);
// store.addProduct(2, "Bread", 3);
// store.addProduct(3, "Eggs Tray", 5);
// store.addProduct(4, "Sugar", 2);
// store.addProduct(5, "Salt", 2);
// store.addProduct(6, "Coffee", 2);

// store.addOrder(123, 1, 2, 4);
// store.addOrder(234, 2, 3);
// store.addOrder(345, 3, 4, 5, 6);
// store.addOrder(456, 2, 3, 6);

// store.save();
//#endregion

//#region Option 2
store.load(`{"customers":[{"id":123,"name":"Liat","address":"Jerusalem"},{"id":234,"name":"Tomer","address":"Giv'at Shmuel"},{"id":345,"name":"Ma'ayan","address":"Tel Aviv"},{"id":456,"name":"Inbar","address":"Ra'anana"}],"products":[{"id":1,"name":"Milk","itemsInStock":1},{"id":2,"name":"Bread","itemsInStock":0},{"id":3,"name":"Eggs Tray","itemsInStock":2},{"id":4,"name":"Sugar","itemsInStock":0},{"id":5,"name":"Salt","itemsInStock":1},{"id":6,"name":"Coffee","itemsInStock":0}],"orders":[{"customerId":123,"productIds":[1,2,4]},{"customerId":234,"productIds":[2,3]},{"customerId":345,"productIds":[3,4,5,6]},{"customerId":456,"productIds":[2,3,6]}]}`);
//#endregion

store.notify();
store.printOrders();
