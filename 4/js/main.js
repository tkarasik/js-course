let matrix = [];
let gameOn = false;
let gen = 0;
let intervalId;

function resetUI() {
  matrix = [];
  gen = 0;
  $("#gen").text(gen);
  stopGame();
}

function initMatrix() {
  resetUI();
  const rows = +$("#rows").val() || 0;
  const columns = +$("#columns").val() || 0;

  if (rows > 0 && columns > 0) {
    for (var i = 0; i < rows; i++) {
      matrix[i] = [];
      for (var j = 0; j < columns; j++) {
        matrix[i][j] = false;
      }
    }
    drawMatrix();
    showGamePanel();
  } else {
    alert(
      "Please enter numberic greater than zero values for rows and columns"
    );
  }
}

function drawMatrix() {
  const rows = +$("#rows").val() || 0;
  const columns = +$("#columns").val() || 0;

  if (rows > 0 && columns > 0) {
    const table = $("#matrix");
    table.empty();
    for (var i = 0; i < rows; i++) {
      row = $("<tr/>");
      for (var j = 0; j < columns; j++) {
        var cell = $("<td/>", {
          id: getCellIdByPosition(i, j),
          row: i,
          column: j,
        }).click((event) => toggleCell(event.target));
        if (matrix[i][j]) cell.attr("alive", "alive");
        row.append(cell);
      }
      table.append(row);
    }
  } else {
    alert("Please enter numberic greater than zero values for rows and columns");
  }
}

function showGamePanel() {
  $("#gamePanel").show();
}

function getCellIdByPosition(row, column) {
  return `cell_${row}_${column}`;
}

function updateCellUI(cell, alive) {
  if (alive) {
    cell.setAttribute("alive", "alive");
  } else {
    cell.removeAttribute("alive");
  }
}

function toggleCell(cell) {
  if (gameOn) return;
  let row = cell.getAttribute("row");
  let column = cell.getAttribute("column");
  let alive = !matrix[row][column];
  matrix[row][column] = alive;
  updateCellUI(cell, alive);
}

function nextGeneration() {
  gen++;
  $("#gen").text(gen);
  let nextGenMatrix = [];
  for (var i = 0; i < matrix.length; i++) {
    nextGenMatrix[i] = [];
    for (var j = 0; j < matrix[i].length; j++) {
      let alive = calculateCell(i, j);
      let cell = document.getElementById(getCellIdByPosition(i, j));
      nextGenMatrix[i][j] = alive;
      updateCellUI(cell, alive);
    }
  }
  matrix = nextGenMatrix;
}

function calculateCell(row, column) {
  let aliveNeighbours = 0;
  for (let angle = 0; angle < 2 * Math.PI; angle += Math.PI / 4) {
    let neighbourRow = row + Math.round(Math.sin(angle));
    let neighbourColumn = column + Math.round(Math.cos(angle));

    if (matrix[neighbourRow] && matrix[neighbourRow][neighbourColumn])
      aliveNeighbours++;
  }
  if (aliveNeighbours < 2 || aliveNeighbours > 3) return false;
  if (aliveNeighbours === 3) return true;
  return matrix[row][column];
}

function startGame() {
  gameOn = true;
  $("#start").hide();
  $("#stop").show();
  intervalId = setInterval(() => {
    nextGeneration();
  }, 1000);
}

function stopGame() {
  gameOn = false;
  $("#start").show();
  $("#stop").hide();
  clearInterval(intervalId);
}
