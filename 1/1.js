for (let i = 1; i <= 100; i++) {
	if (isMultipleOfAny(i, 3, 5)) {
		console.log(i + ":\t" + (isMultipleOfAny(i, 3) ? "Fizz" : "") + (isMultipleOfAny(i, 5) ? "Buzz" : ""));
	}
}

function isMultipleOfAny(num, ...multipliers) {
	for (multiplier of multipliers) {
		if (!(num % multiplier)) return true;
	}
	return false;
}
