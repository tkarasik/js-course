let numToPromise = async (num) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (num > 10) {
        resolve(true);
      } else {
        reject("The number has to be greater than 10!");
      }
    }, 500);
  });
};

//#region Promise Chainable API
let promiseWrapper1 = (num) => numToPromise(num).then(
    (resolved) => console.log(`Wrapeer 1 resolved input ${num}: ${resolved}`), 
    (rejected) => console.error(`Wrapeer 1 rejected input ${num}: ${rejected}`));

promiseWrapper1(10);
promiseWrapper1(11);
//#endregion

//#region Async-Await
let promiseWrapper2 = async (num) => {
    let result = await numToPromise(num).catch((error) => console.error(`Wrapeer 2 for input ${num} threw: ${error}`));
    if (result) console.log(`Wrapeer 2 for input ${num} returned: ${result}`);
}

promiseWrapper2(10);
promiseWrapper2(11);
//#endregion
